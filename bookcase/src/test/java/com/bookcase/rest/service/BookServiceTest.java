package com.bookcase.rest.service;

import com.bookcase.beans.Book;
import com.bookcase.beans.Shelve;
import com.bookcase.dto.BookDTO;
import com.bookcase.exceptions.NotFoundException;
import com.bookcase.repository.BookRepository;
import com.bookcase.repository.ShelveRepository;
import com.bookcase.service.impl.BookServiceImpl;
import com.bookcase.service.mapper.BookMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BookServiceTest {

    @Mock
    BookRepository bookRepository;

    @Mock
    ShelveRepository shelveRepository;

    @Mock
    BookMapper bookMapper;

    @InjectMocks
    BookServiceImpl bookService;

    @AfterEach
    void tearDown() {

        verifyNoMoreInteractions(bookRepository, shelveRepository);
    }

    @Test
    void findAll() {
        Shelve shelve = new Shelve();
        Book book = new Book();
        BookDTO bookDto = new BookDTO();
        List<Book> books = Collections.singletonList(book);
        List<BookDTO> booksDtos = Collections.singletonList(bookDto);

        when(shelveRepository.findById(1L)).thenReturn(Optional.of(shelve));
        when(bookRepository.findByShelve(shelve)).thenReturn(books);
        when(bookMapper.entitiesToDTOs(books)).thenReturn(booksDtos);

        assertSame(booksDtos, bookService.findAll(1L));

        verify(shelveRepository).findById(1L);
        verify(bookRepository).findByShelve(shelve);
        verify(bookMapper).entitiesToDTOs(books);
    }

    @Test
    void getById() {
        Book book = new Book();
        Shelve shelve = new Shelve();
        shelve.setId(1L);
        book.setShelve(shelve);
        BookDTO bookDto = new BookDTO();

        when(bookRepository.findById(1L)).thenReturn(Optional.of(book));
        when(bookMapper.entityToDto(book)).thenReturn(bookDto);

        assertSame(bookDto, bookService.findById(1L, 1L));

        verify(bookRepository).findById(1L);
        verify(bookMapper).entityToDto(book);
    }

    @Test
    void getByIdNotFound() {

        when(bookRepository.findById(1L)).thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> bookService.findById(1L,1L));

        verify(bookRepository).findById(1L);
    }

    @Test
    void create() {
        Book book = new Book();
        Shelve shelve = new Shelve();
        shelve.setId(1L);
        book.setShelve(shelve);
        BookDTO bookDto = new BookDTO();

        when(bookMapper.dtoToEntity(bookDto)).thenReturn(book);
        when(bookRepository.save(book)).thenReturn(book);

        assertSame(bookDto, bookService.create(bookDto));

        verify(bookMapper).dtoToEntity(bookDto);
        verify(bookRepository).save(book);
    }

    @Test
    void update() {
        Book book = new Book();
        Shelve shelve = new Shelve();
        shelve.setId(1L);
        book.setShelve(shelve);
        BookDTO bookDto = new BookDTO();

        when(bookMapper.dtoToEntity(bookDto)).thenReturn(book);
        when(bookRepository.save(book)).thenReturn(book);

        assertSame(bookDto, bookService.create(bookDto));

        verify(bookMapper).dtoToEntity(bookDto);
        verify(bookRepository).save(book);
    }


    @Test
    void deleteById() {
        Book book = new Book();

        bookRepository.delete(book);

        verify(bookRepository).delete(book);
    }

    @Test
    void deleteNotFound() {
        when(bookRepository.findById(1L)).thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> bookService.findById(1L,1L));

        verify(bookRepository).findById(1L);
    }

}
