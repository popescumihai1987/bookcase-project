/*
 * Copyright (c) Orange Services 2020, All Rights Reserved.
 *
 * This software is the confidential and proprietary information of Orange Services.
 * You shall not disclose such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with Orange Services.
 */

package com.bookcase.rest.controller;

import com.bookcase.controller.BookController;
import com.bookcase.dto.BookDTO;
import com.bookcase.service.BookService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BookControllerTest {

    @Mock
    BookService bookService;

    @InjectMocks
    BookController bookController;

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(bookService);
    }

    @Test
    void getAll() {
        List<BookDTO> expectedList = Collections.singletonList(new BookDTO());
        when(bookController.getAll(1L)).thenReturn(expectedList);
        Assertions.assertEquals(expectedList, bookController.getAll(1L));
        verify(bookService).findAll(1L);
    }

    @Test
    void getById() {
        bookController.getById(1L, 1L);
        verify(bookService).findById(1L, 1L);
    }

    @Test
    void create() {
        BookDTO expected = new BookDTO();
        bookController.create(expected);
        verify(bookService).create(expected);
    }

    @Test
    void update() {
        BookDTO expected = new BookDTO();
        bookController.update(1L, expected);
        verify(bookService).update(expected);
    }

    @Test
    void delete() {
        bookController.delete(1L);
        verify(bookService).deleteById(1L);
    }

}
