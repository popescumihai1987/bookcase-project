package com.bookcase.service.mapper;

import com.bookcase.beans.Book;
import com.bookcase.dto.BookDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Component
public class BookMapper {

    final ShelveMapper shelveMapper;

    public List<BookDTO> entitiesToDTOs(List<Book> entityList) {
        List<BookDTO> dtoList = new ArrayList<>();
        entityList.forEach(book -> {
                    BookDTO bookDTO = new BookDTO();
                    bookDTO.setAuthor(book.getAuthor());
                    bookDTO.setDescription(book.getDescription());
                    bookDTO.setShelve(book.getShelve() != null ? shelveMapper.entityToDto(book.getShelve()) : null);
                    bookDTO.setTitle(book.getTitle());
                    dtoList.add(bookDTO);
                });
        return dtoList;
    }

    public BookDTO entityToDto(Book entity) {
        BookDTO dto = new BookDTO();
        dto.setId(entity.getId());
        dto.setTitle(entity.getTitle());
        dto.setDescription(entity.getDescription());
        dto.setAuthor(entity.getAuthor());
        dto.setShelve(shelveMapper.entityToDto(entity.getShelve()));
        return dto;

    }

    public Book dtoToEntity(BookDTO dto) {
        Book book = new Book();
        book.setTitle(dto.getTitle());
        book.setDescription(dto.getDescription());
        book.setAuthor(dto.getAuthor());
        book.setShelve(shelveMapper.dtoToEntity(dto.getShelve()));
        return book;
    }

}
