package com.bookcase.service.mapper;

import com.bookcase.beans.BookCase;
import com.bookcase.dto.BookCaseDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Component
public class BookCaseMapper {

    public List<BookCaseDTO> entitiesToDTOs(List<BookCase> entityList) {
        List<BookCaseDTO> bookCaseDTOList = new ArrayList<>();
        entityList.forEach(bookCase -> bookCaseDTOList.add(entityToDto(bookCase)));
        return bookCaseDTOList;
    }

    public BookCaseDTO entityToDto(BookCase entity) {
        BookCaseDTO dto = new BookCaseDTO();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        return dto;
    }

    public BookCase dtoToEntity(BookCaseDTO dto) {
        BookCase bookCase = new BookCase();
        bookCase.setId(dto.getId() != null ? dto.getId() : null);
        bookCase.setName(dto.getName());
        return bookCase;
    }

}
