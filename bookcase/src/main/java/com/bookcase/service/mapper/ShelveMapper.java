package com.bookcase.service.mapper;

import com.bookcase.beans.Shelve;
import com.bookcase.dto.ShelveDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Component
public class ShelveMapper {

    final BookCaseMapper bookCaseMapper;

    public List<ShelveDTO> entitiesToDTOs(List<Shelve> entityList) {
        List<ShelveDTO> shelveDTOS = new ArrayList<>();
        entityList.forEach(shelve -> shelveDTOS.add(entityToDto(shelve)));
        return shelveDTOS;
    }

    public ShelveDTO entityToDto(Shelve entity) {
        ShelveDTO shelveDTO = new ShelveDTO();
        shelveDTO.setId(entity.getId());
        shelveDTO.setName(entity.getName());
        shelveDTO.setBookCase(bookCaseMapper.entityToDto(entity.getBookCase()));
        return shelveDTO;
    }

    public Shelve dtoToEntity(ShelveDTO dto) {
        Shelve shelve = new Shelve();
        shelve.setName(dto.getName());
        shelve.setBookCase(bookCaseMapper.dtoToEntity(dto.getBookCase()));
        shelve.setId(dto.getId());
        return shelve;
    }

}
