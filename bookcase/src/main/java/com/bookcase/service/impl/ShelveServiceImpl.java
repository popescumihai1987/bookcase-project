package com.bookcase.service.impl;

import com.bookcase.beans.Book;
import com.bookcase.beans.BookCase;
import com.bookcase.beans.Shelve;
import com.bookcase.dto.BookCaseDTO;
import com.bookcase.dto.ShelveDTO;
import com.bookcase.exceptions.NotFoundException;
import com.bookcase.repository.BookCaseRepository;
import com.bookcase.repository.BookRepository;
import com.bookcase.repository.ShelveRepository;
import com.bookcase.service.ShelveService;
import com.bookcase.service.mapper.ShelveMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ShelveServiceImpl implements ShelveService {

    private final BookCaseRepository bookCaseRepository;

    private final ShelveRepository shelveRepository;

    private final BookRepository bookRepository;

    private final ShelveMapper shelveMapper;

    @Autowired
    public ShelveServiceImpl(BookCaseRepository bookCaseRepository,
                             ShelveRepository shelveRepository,
                             BookRepository bookRepository,
                             ShelveMapper shelveMapper) {
        this.bookCaseRepository = bookCaseRepository;
        this.shelveRepository = shelveRepository;
        this.bookRepository = bookRepository;
        this.shelveMapper = shelveMapper;
    }

    @Override
    public ShelveDTO create(ShelveDTO shelveDTO) {
        Shelve shelve = shelveMapper.dtoToEntity(shelveDTO);
        Shelve newShelve = shelveRepository.save(shelve);
        shelveDTO.setId(newShelve.getId());
        return shelveDTO;
    }

    @Override
    public ShelveDTO update(ShelveDTO shelveDTO) {
        Shelve shelveToUpdate = shelveMapper.dtoToEntity(shelveDTO);
        shelveRepository.save(shelveToUpdate);
        return shelveDTO;
    }

    @Override
    public List<ShelveDTO> findAll(Long bookCaseId) {
        BookCase bookCase = bookCaseRepository.findById(bookCaseId).orElseThrow(NotFoundException::new);
        return shelveMapper.entitiesToDTOs
                (shelveRepository.findByBookCase(bookCase));
    }

    @Override
    public ShelveDTO findById(Long bookCaseId, Long id) {
        Shelve shelve = shelveRepository.findById(id).orElseThrow(NotFoundException::new);
        if (!shelve.getBookCase().getId().equals(bookCaseId)) {
            return null;
        }
        return shelveMapper.entityToDto(shelve);
    }

    @Override
    public void deleteById(Long id) {
        Shelve shelve = shelveRepository.findById(id).orElseThrow(NotFoundException::new);
        List<Book> books = bookRepository.findByShelve(shelve);
        for (Book book : books) {
            book.setShelve(null);
            bookRepository.save(book);
        }
        BookCase bookCase = bookCaseRepository.getById(shelve.getBookCase().getId());
        bookCase.setShelves(
                bookCase.getShelves().stream()
                        .filter(shelve1 -> shelve1.getId().equals(id))
                        .collect(Collectors.toList()));
        bookCaseRepository.save(bookCase);
        shelveRepository.delete(shelve);
    }

    @Override
    public List<ShelveDTO> findByBookCase(BookCaseDTO bookCaseDTO) {
        List<Shelve> shelveList = shelveRepository
                .findByBookCase(bookCaseRepository.getById(bookCaseDTO.getId()));
        return shelveMapper.entitiesToDTOs(shelveList);
    }
}
