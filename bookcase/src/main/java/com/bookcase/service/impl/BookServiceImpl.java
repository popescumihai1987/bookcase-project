package com.bookcase.service.impl;

import com.bookcase.beans.Book;
import com.bookcase.beans.Shelve;
import com.bookcase.dto.BookDTO;
import com.bookcase.dto.ShelveDTO;
import com.bookcase.exceptions.NotFoundException;
import com.bookcase.repository.BookRepository;
import com.bookcase.repository.ShelveRepository;
import com.bookcase.service.BookService;
import com.bookcase.service.mapper.BookMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;
    private final ShelveRepository shelveRepository;

    private final BookMapper bookMapper;


    @Autowired
    public BookServiceImpl(BookMapper bookMapper,
                           ShelveRepository shelveRepository,
                           BookRepository bookRepository) {
        this.bookRepository = bookRepository;
        this.shelveRepository = shelveRepository;
        this.bookMapper = bookMapper;
    }

    @Override
    public BookDTO create(BookDTO bookDTO) {
        Book book = bookMapper.dtoToEntity(bookDTO);
        Book newBook = bookRepository.save(book);
        bookDTO.setId(newBook.getId());
        return bookDTO;
    }

    @Override
    public BookDTO update(BookDTO bookDTO) {
        Book bookToUpdate = bookMapper.dtoToEntity(bookDTO);
        bookRepository.save(bookToUpdate);
        return bookDTO;
    }

    @Override
    public List<BookDTO> findAll(Long shelveId) {
        Shelve shelve = shelveRepository.findById(shelveId).orElseThrow(NotFoundException::new);
        return bookMapper.entitiesToDTOs(bookRepository.findByShelve(shelve));
    }

    @Override
    public BookDTO findById(Long shelveId, Long id) {
        Book book = bookRepository.findById(id).orElseThrow(NotFoundException::new);
        if (!book.getShelve().getId().equals(shelveId)) {
            return null;
        }
        return bookMapper.entityToDto(book);
    }

    @Override
    public List<BookDTO> findByShelve(ShelveDTO shelve) {
        Shelve shelveDb = shelveRepository.getById(shelve.getId());
        return bookMapper.entitiesToDTOs(
                bookRepository.findByShelve(shelveDb));
    }

    @Override
    public void deleteById(Long id) {
        Book book = bookRepository.findById(id).orElseThrow(NotFoundException::new);
        bookRepository.delete(book);
    }

    @Override
    public void addToShelve(Long id, Long shelveId) {
        Book book = bookRepository.findById(id).orElseThrow(NotFoundException::new);
        Shelve shelve = shelveRepository.findById(shelveId).orElseThrow(NotFoundException::new);
        book.setShelve(shelve);
        bookRepository.save(book);
    }

    @Override
    public List<BookDTO> findAllLoose() {
        return bookMapper.entitiesToDTOs(bookRepository.findByShelve(null));
    }
}
