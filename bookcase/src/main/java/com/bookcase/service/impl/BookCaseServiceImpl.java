package com.bookcase.service.impl;

import com.bookcase.beans.Book;
import com.bookcase.beans.BookCase;
import com.bookcase.beans.Shelve;
import com.bookcase.dto.BookCaseDTO;
import com.bookcase.exceptions.NotFoundException;
import com.bookcase.repository.BookCaseRepository;
import com.bookcase.repository.BookRepository;
import com.bookcase.repository.ShelveRepository;
import com.bookcase.service.BookCaseService;
import com.bookcase.service.mapper.BookCaseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class BookCaseServiceImpl implements BookCaseService {

    private final BookCaseRepository bookCaseRepository;

    private final ShelveRepository shelveRepository;

    private final BookRepository bookRepository;

    private final BookCaseMapper mapper;

    @Autowired
    public BookCaseServiceImpl(BookCaseRepository bookCaseRepository,
                               BookRepository bookRepository,
                               ShelveRepository shelveRepository,
                               BookCaseMapper mapper) {
        this.bookCaseRepository = bookCaseRepository;
        this.shelveRepository = shelveRepository;
        this.bookRepository = bookRepository;
        this.mapper = mapper;
    }

    @Override
    public BookCaseDTO create(BookCaseDTO bookCaseDTO) {
        BookCase bookCase = mapper.dtoToEntity(bookCaseDTO);
        BookCase newBookCase = bookCaseRepository.save(bookCase);
        bookCaseDTO.setId(newBookCase.getId());
        return bookCaseDTO;
    }

    @Override
    public BookCaseDTO update(BookCaseDTO bookCaseDTO) {
        BookCase bookCaseToUpdate = mapper.dtoToEntity(bookCaseDTO);
        bookCaseRepository.save(bookCaseToUpdate);
        return bookCaseDTO;
    }

    @Override
    public List<BookCaseDTO> findAll() {
        return mapper.entitiesToDTOs(bookCaseRepository.findAll());
    }

    @Override
    public BookCaseDTO findById(Long id) {
        BookCase bookCase = bookCaseRepository.findById(id).orElseThrow(NotFoundException::new);
        return mapper.entityToDto(bookCase);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        BookCase bookCase = bookCaseRepository.findById(id).orElseThrow(NotFoundException::new);
        List<Shelve> shelves = shelveRepository.findByBookCase(bookCase);
        for (Shelve shelve : shelves) {
            List<Book> books = bookRepository.findByShelve(shelve);
            for (Book book : books) {
                book.setShelve(null);
                bookRepository.save(book);
            }
        }
        shelveRepository.deleteByBookCase(bookCase);
        bookCaseRepository.delete(bookCase);
    }
}
