package com.bookcase.service;

import com.bookcase.dto.BookDTO;
import com.bookcase.dto.ShelveDTO;

import java.util.List;

public interface BookService {

    BookDTO create(BookDTO shelveDTO);

    BookDTO update(BookDTO shelveDTO);

    List<BookDTO> findAll(Long shelveId);

    BookDTO findById(Long shelveId, Long id);

    List<BookDTO> findByShelve(ShelveDTO shelve);

    void deleteById(Long id);

    void addToShelve(Long id, Long shelveId);

    List<BookDTO> findAllLoose();
}
