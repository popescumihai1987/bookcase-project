package com.bookcase.service;

import com.bookcase.dto.BookCaseDTO;

import java.util.List;

public interface BookCaseService {

    BookCaseDTO create(BookCaseDTO bookCaseDTO);

    BookCaseDTO update(BookCaseDTO bookCaseDTO);

    List<BookCaseDTO> findAll();

    BookCaseDTO findById(Long id);

    void deleteById(Long id);
}
