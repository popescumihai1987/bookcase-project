package com.bookcase.service;

import com.bookcase.dto.BookCaseDTO;
import com.bookcase.dto.ShelveDTO;

import java.util.List;

public interface ShelveService {

    ShelveDTO create(ShelveDTO shelveDTO);

    ShelveDTO update(ShelveDTO shelveDTO);

    List<ShelveDTO> findAll(Long bookCaseId);

    List<ShelveDTO> findByBookCase(BookCaseDTO bookCaseDTO);

    ShelveDTO findById(Long bookCaseId, Long id);

    void deleteById(Long id);

}
