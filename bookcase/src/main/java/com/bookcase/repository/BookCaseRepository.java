package com.bookcase.repository;

import com.bookcase.beans.BookCase;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookCaseRepository extends CrudRepository<BookCase, Long> {
    List<BookCase> findAll();
    BookCase getById(Long id);
}
