package com.bookcase.repository;

import com.bookcase.beans.Book;
import com.bookcase.beans.Shelve;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends CrudRepository<Book, Long> {
    List<Book> findAll();
    List<Book> findByShelve(Shelve shelve);
}
