package com.bookcase.repository;

import com.bookcase.beans.BookCase;
import com.bookcase.beans.Shelve;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShelveRepository extends CrudRepository<Shelve, Long> {
    List<Shelve> findAll();
    void deleteByBookCase(BookCase bookCase);
    List<Shelve> findByBookCase(BookCase bookCase);
    Shelve getById(Long id);
}
