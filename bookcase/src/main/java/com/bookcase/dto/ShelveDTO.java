package com.bookcase.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class ShelveDTO {
    private Long id;
    @NotBlank
    private String name;
    private BookCaseDTO bookCase;
}
