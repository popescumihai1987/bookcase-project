package com.bookcase.controller;

import com.bookcase.dto.BookDTO;
import com.bookcase.service.BookService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("http://localhost:4200")
@RestController
@RequestMapping(value = "/loosebooks", produces = "application/json")
public class LooseBookController {

    private final BookService bookService;

    public LooseBookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping()
    public List<BookDTO> getAll() {
        return bookService.findAllLoose();
    }

    @GetMapping("/{bookId}")
    public BookDTO getById(@PathVariable("shelveId") Long shelveId, @PathVariable("bookId") Long bookId) {
        return bookService.findById(shelveId, bookId);
    }

    @PutMapping("/{bookId}")
    public BookDTO update(@PathVariable("bookId") Long bookId, @RequestBody BookDTO bookDTO) {
        bookDTO.setId(bookId);
        return bookService.update(bookDTO);
    }

    @DeleteMapping("/{bookId}")
    public void delete(@PathVariable("bookId") Long bookId) {
        bookService.deleteById(bookId);
    }

}
