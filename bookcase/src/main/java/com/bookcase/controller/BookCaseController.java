package com.bookcase.controller;

import com.bookcase.dto.BookCaseDTO;
import com.bookcase.service.BookCaseService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin("http://localhost:4200")
@RestController
@RequestMapping(value = "/bookcase", produces = "application/json")
public class BookCaseController {

    private final BookCaseService bookCaseService;

    public BookCaseController(BookCaseService bookCaseService) {
        this.bookCaseService = bookCaseService;
    }

    @PostMapping
    public BookCaseDTO create(@RequestBody @Valid BookCaseDTO bookCaseDTO) {
        return bookCaseService.create(bookCaseDTO);
    }

    @GetMapping()
    public List<BookCaseDTO> getAll() {
        return bookCaseService.findAll();
    }

    @GetMapping("/{bookCaseId}")
    public BookCaseDTO getById(@PathVariable("bookCaseId") Long wsId) {
        return bookCaseService.findById(wsId);
    }

    @PutMapping("/{bookCaseId}")
    public BookCaseDTO update(@PathVariable("bookCaseId") Long id, @RequestBody @Valid BookCaseDTO bookCaseDTO) {
        bookCaseDTO.setId(id);
        return bookCaseService.update(bookCaseDTO);
    }

    @DeleteMapping("/{bookCaseId}")
    public void delete(@PathVariable("bookCaseId") Long bookCaseId) {
        bookCaseService.deleteById(bookCaseId);
    }
}
