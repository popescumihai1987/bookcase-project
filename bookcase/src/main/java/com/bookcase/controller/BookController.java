package com.bookcase.controller;

import com.bookcase.dto.BookDTO;
import com.bookcase.service.BookService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin("http://localhost:4200")
@RestController
@RequestMapping(value = "/bookcase/{bookCaseId}/shelve/{shelveId}/books", produces = "application/json")
public class BookController {

    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @PostMapping
    public BookDTO create(@RequestBody @Valid BookDTO bookDTO) {
        return bookService.create(bookDTO);
    }

    @GetMapping()
    public List<BookDTO> getAll(@PathVariable("shelveId") Long shelveId) {
        return bookService.findAll(shelveId);
    }

    @GetMapping("/{bookId}")
    public BookDTO getById(@PathVariable("shelveId") Long shelveId, @PathVariable("bookId") Long bookId) {
        return bookService.findById(shelveId, bookId);
    }

    @PutMapping("/{bookId}")
    public BookDTO update(@PathVariable("bookId") Long bookId, @RequestBody @Valid BookDTO bookDTO) {
        bookDTO.setId(bookId);
        return bookService.update(bookDTO);
    }

    @DeleteMapping("/{bookId}")
    public void delete(@PathVariable("bookId") Long bookId) {
        bookService.deleteById(bookId);
    }

}
