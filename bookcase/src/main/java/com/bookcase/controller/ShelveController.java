package com.bookcase.controller;

import com.bookcase.dto.ShelveDTO;
import com.bookcase.service.ShelveService;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;

@CrossOrigin("http://localhost:4200")
@RestController
@RequestMapping(value = "/bookcase/{bookCaseId}/shelve", produces = "application/json")
public class ShelveController {

    private final ShelveService shelveService;

    public ShelveController(ShelveService shelveService) {
        this.shelveService = shelveService;
    }

    @PostMapping
    public ShelveDTO create(@RequestBody @Valid ShelveDTO shelveDTO) {
        return shelveService.create(shelveDTO);
    }

    @GetMapping()
    public List<ShelveDTO> getAll(@PathVariable("bookCaseId") Long bookCaseId) {
        return shelveService.findAll(bookCaseId);
    }

    @GetMapping("/{shelveId}")
    public ShelveDTO getById(@PathVariable("bookCaseId") Long bookCaseId, @PathVariable("shelveId") Long shelveId) {
        return shelveService.findById(bookCaseId, shelveId);
    }

    @PutMapping("/{shelveId}")
    public ShelveDTO update(@PathVariable("shelveId") Long shelveId, @RequestBody @Valid ShelveDTO shelveDTO) {
        shelveDTO.setId(shelveId);
        return shelveService.update(shelveDTO);
    }

    @DeleteMapping("/{shelveId}")
    public void delete(@PathVariable("shelveId") Long shelveId) {
        shelveService.deleteById(shelveId);
    }
}
