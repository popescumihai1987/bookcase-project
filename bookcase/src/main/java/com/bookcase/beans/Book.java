package com.bookcase.beans;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "BOOKS")
@Getter
@Setter
@NoArgsConstructor
public class Book {
    @Column(name = "ID")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "AUTHOR")
    private String author;

    @Column(name = "DESCRIPTION")
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    private Shelve shelve;
}
