package com.bookcase.beans;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "BOOKCASES")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BookCase {
    @Column(name = "ID")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "NAME", unique = true)
    private String name;

    @OneToMany(
            mappedBy = "bookCase",
            cascade = CascadeType.DETACH
    )
    private List<Shelve> shelves = new ArrayList<>();
}
