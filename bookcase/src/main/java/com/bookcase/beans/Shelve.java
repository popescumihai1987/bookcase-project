package com.bookcase.beans;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "SHELVES")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Shelve {
    @Column(name = "ID")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "NAME", unique = true)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    private BookCase bookCase;
}
