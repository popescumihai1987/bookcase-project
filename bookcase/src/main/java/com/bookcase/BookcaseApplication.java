package com.bookcase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.bookcase.repository")
public class BookcaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookcaseApplication.class, args);
	}

}
