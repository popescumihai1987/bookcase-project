CREATE DATABASE IF NOT EXISTS bookcaseprojectdb;

CREATE USER 'bookcaseuser'@'localhost' IDENTIFIED BY 'bookcasepassword';

GRANT ALL PRIVILEGES ON * . * TO 'bookcaseuser'@'localhost';

FLUSH PRIVILEGES;