import {ComponentFixture, TestBed} from '@angular/core/testing';
import {LooseBookDialogComponent} from './book-dialog.component';

describe('BookDialogComponent', () => {
  let component: LooseBookDialogComponent;
  let fixture: ComponentFixture<LooseBookDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LooseBookDialogComponent ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LooseBookDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
