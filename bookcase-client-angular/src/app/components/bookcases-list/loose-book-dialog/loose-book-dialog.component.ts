import {Component, forwardRef, Inject, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, NG_VALUE_ACCESSOR, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {BookcaseService} from '../../../services/bookcase.service';
import {Shelve} from '../../../common/shelve';
import {ShelveService} from '../../../services/shelve.service';
import {BookService} from '../../../services/book.service';
import {Book} from '../../../common/book';
import {LooseBookService} from '../../../services/loose-books.service';
import {Bookcase} from '../../../common/bookcase';

@Component({
  selector: 'app-shelve-create',
  templateUrl: './loose-book-dialog.component.html',
  styleUrls: ['./loose-book-dialog.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => BookcaseService),
    }
  ]
})
export class LooseBookDialogComponent implements OnInit {
  bookForm: FormGroup;

  selectedShelve: Shelve;
  selectedBookCase: Bookcase = null;

  bookCases = [];
  bookcaseId: number;

  shelves = [];

  books: Book[];

  constructor(@Inject(MAT_DIALOG_DATA) public data: Book,
              private formBuilder: FormBuilder,
              private bookcaseService: BookcaseService,
              private looseBookService: LooseBookService,
              private shelveService: ShelveService,
              private bookService: BookService,
              private snackBar: MatSnackBar) {}

  ngOnInit(): void {

      this.bookcaseService.getAll().subscribe(data => this.bookCases = data);
      this.initForm(this.data);
  }

  private initForm(book: Book): void {
    this.bookForm = this.formBuilder.group({
      bookTitle: [book.title, Validators.required],
      bookAuthor: [book.author, Validators.required],
      bookDesc: [book.description, Validators.required],
      bookCase: [this.selectedBookCase],
      bookShelve: [null]
    });
  }

  get f(): { [key: string]: AbstractControl } {
    return this.bookForm.controls;
  }

  onSubmit(): void {
    const updateBook = new Book();
    updateBook.id = this.data.id;
    updateBook.title = this.bookForm.value.bookTitle;
    updateBook.author = this.bookForm.value.bookAuthor;
    updateBook.description = this.bookForm.value.bookDesc;
    if (this.data.shelve != null) {
      this.looseBookService.updateShelve(this.data.shelve.id, updateBook)
        .subscribe(() => {
          this.snackBar.open('The book has been added to shelve ' + this.data.shelve.name + '!', 'OK');
        });
    }
    else {
      this.looseBookService.update(updateBook)
        .subscribe(() => {
          this.snackBar.open('The book has been successfully updated !', 'OK');
        });
    }
  }

  onChange(selection): void {
    this.shelveService.getAll(selection.id).subscribe(data => this.shelves = data);
  }
}
