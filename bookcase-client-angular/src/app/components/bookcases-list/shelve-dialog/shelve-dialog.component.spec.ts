import {ComponentFixture, TestBed} from '@angular/core/testing';
import {ShelveDialogComponent} from './shelve-dialog.component';

describe('BookcasesDialogComponent', () => {
  let component: ShelveDialogComponent;
  let fixture: ComponentFixture<ShelveDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShelveDialogComponent ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShelveDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
