import {Component, Inject, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {Bookcase} from "../../../common/bookcase";
import {BookcaseService} from "../../../services/bookcase.service";
import {Shelve} from "../../../common/shelve";
import {ShelveService} from "../../../services/shelve.service";

@Component({
  selector: 'app-shelve-create',
  templateUrl: './shelve-dialog.component.html',
  styleUrls: ['./shelve-dialog.component.scss']
})
export class ShelveDialogComponent implements OnInit {
  shelveForm: FormGroup;

  bookcaseId: number;
  bookcaseName: string = this.data.bookCase.name;
  bookcase: Bookcase;

  bookcases: Bookcase[];

  constructor(@Inject(MAT_DIALOG_DATA) public data: Shelve,
              private formBuilder: FormBuilder,
              private bookcaseService: BookcaseService,
              private shelveService: ShelveService,
              private snackBar: MatSnackBar) {
    this.bookcaseService.getAll().subscribe( result => {
      this.bookcases = result;
    })
  }

  ngOnInit(): void {
      this.initForm(this.data);
  }

  private initForm(shelve: Shelve): void {
    console.info(this.data);
    this.shelveForm = this.formBuilder.group({
      shelveName: [shelve.name, Validators.required]
    });
  }

  changeBookcase(bookcase: Bookcase) {
    this.bookcaseService.getById(bookcase.id).subscribe(result =>
      this.bookcaseId = result.id
    );
  }

  get f(): { [key: string]: AbstractControl } {
    return this.shelveForm.controls;
  }

  onSubmit(): void {
    let updateShelve = new Shelve();
    updateShelve.id = this.data.id;
    updateShelve.name = this.shelveForm.value.shelveName;
    let bookcase = new Bookcase();
    bookcase.id = this.data.bookCase.id;
    updateShelve.bookCase = bookcase;
    this.shelveService.update(this.data.bookCase.id, updateShelve)
      .subscribe(() => {
        this.snackBar.open('Shelve has been successfully updated !', 'OK');
      });
  }
}
