import {ComponentFixture, TestBed} from '@angular/core/testing';
import {BookcaseDialogComponent} from './bookcase-dialog.component';

describe('BookcaseDialogComponent', () => {
  let component: BookcaseDialogComponent;
  let fixture: ComponentFixture<BookcaseDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookcaseDialogComponent ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookcaseDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
