import {Component, Inject, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {Bookcase} from '../../../common/bookcase';
import {BookcaseService} from '../../../services/bookcase.service';

@Component({
  selector: 'app-bookcase-create',
  templateUrl: './bookcase-dialog.component.html',
  styleUrls: ['./bookcase-dialog.component.scss']
})
export class BookcaseDialogComponent implements OnInit {
  bookcaseForm: FormGroup;

  bookcaseId: number = this.data.id;

  constructor(@Inject(MAT_DIALOG_DATA) public data: Bookcase,
              private formBuilder: FormBuilder,
              private bookcaseService: BookcaseService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
      this.initForm(this.data);
  }

  private initForm(bookcase: Bookcase): void {
    this.bookcaseForm = this.formBuilder.group({
      bookcaseName: [bookcase.name, Validators.required]
    });
  }

  get f(): { [key: string]: AbstractControl } {
    return this.bookcaseForm.controls;
  }

  onSubmit(): void {
    const updateBookcase = new Bookcase();
    updateBookcase.id = this.data.id;
    updateBookcase.name = this.bookcaseForm.value.bookcaseName;
    this.bookcaseService.update(updateBookcase)
      .subscribe(() => {
        this.snackBar.open('Bookcase has been successfully updated !', 'OK');
      });
  }
}
