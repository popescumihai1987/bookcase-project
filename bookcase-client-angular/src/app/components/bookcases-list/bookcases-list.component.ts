import {Component, OnInit} from '@angular/core';
import {BookcaseService} from '../../services/bookcase.service';
import {Bookcase} from '../../common/bookcase';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Shelve} from '../../common/shelve';
import {ShelveService} from '../../services/shelve.service';
import {Book} from '../../common/book';
import {BookService} from '../../services/book.service';
import {DeleteDialogComponent} from '../dialog/delete/delete-dialog.component';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {BookcaseDialogComponent} from './bookcase-dialog/bookcase-dialog.component';
import {ShelveDialogComponent} from './shelve-dialog/shelve-dialog.component';
import {BookDialogComponent} from './book-dialog/book-dialog.component';

@Component({
  selector: 'app-bookcases-list',
  templateUrl: './bookcases-list.component.html',
  styleUrls: ['./bookcases-list.component.css']
})
export class BookcasesListComponent implements OnInit {

  bookcases: Bookcase[];

  bookCaseName: string;

  shelves: Shelve[];

  shelveName: string;

  bookCaseId: number;

  books: Book[];

  bookTitle: string;
  bookAuthor: string;
  bookDescription: string;

  constructor(private bookcaseService: BookcaseService,
              private shelveService: ShelveService,
              private bookService: BookService,
              private deleteDialog: MatDialog,
              private validationDialog: MatDialog,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.listBookcases();
  }

  listBookcases(): void {
    this.bookcaseService.getAll().subscribe(
      data => {
        this.bookcases = data;
      }
    );
  }

  listShelves(bookCaseId: number): void {
    this.shelveService.getAll(bookCaseId).subscribe(
      data => {
        this.shelves = data;
      }
    );
  }

  listBooks(bookCaseId: number, shelveId: number): void {
    this.bookService.getAll(bookCaseId, shelveId).subscribe(
      data => {
        this.books = data;
      }
    );
  }

  onSubmitBookCase(): void {
   const bookcase = new Bookcase();
   bookcase.id = 0;
   bookcase.name = this.bookCaseName;
   this.bookcaseService.add(bookcase).subscribe( () => {
       this.snackBar.open('Bookcase has been successfully created !', 'OK');
       this.listBookcases();
     }
   );
  }

  onSubmitShelve(bookcaseId: number): void {
    const shelve = new Shelve();
    shelve.name = this.shelveName;
    const bookCase = new Bookcase();
    bookCase.id = bookcaseId;
    shelve.bookCase = bookCase;
    this.shelveService.add(bookcaseId, shelve).subscribe( () => {
        this.snackBar.open('Shelve has been successfully created !', 'OK');
        this.listShelves(bookcaseId);
      }
    );
  }

  onSubmitBook(bookcaseId: number, shelveId: number): void {
    const book = new Book();
    book.title = this.bookTitle;
    book.author = this.bookAuthor;
    book.description = this.bookDescription;
    const shelve = new Shelve();
    shelve.id = shelveId;
    const bookCase = new Bookcase();
    bookCase.id = bookcaseId;
    shelve.bookCase = bookCase;
    book.shelve = shelve;
    this.bookService.add(bookcaseId, shelveId, book).subscribe( () => {
        this.snackBar.open('Book has been successfully created !', 'OK');
        this.listBooks(bookcaseId, shelveId);
      }
    );
  }

  private deleteBookCase(bookCaseId: number): void {
    this.bookcaseService.delete(bookCaseId)
      .subscribe(() => {
        this.snackBar.open('Bookcase has been successfully deleted !', 'OK');
        this.listBookcases();
      });
  }

  onDeleteBookCase(bookCaseId: number): void {
    const dialogRef = this.deleteDialog.open(DeleteDialogComponent, {
        width: '250px',
        data: 'Bookcase'
      });

    dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.deleteBookCase(bookCaseId);
        }
      });
    }

  private deleteShelve(bookCaseId: number, shelveId: number): void {
    this.shelveService.delete(bookCaseId, shelveId)
      .subscribe(() => {
        this.snackBar.open('Shelve has been successfully deleted !', 'OK');
        this.listShelves(bookCaseId);
      });
  }

  onDeleteShelve(bookCaseId: number, shelveId: number): void {
    const dialogRef = this.deleteDialog.open(DeleteDialogComponent, {
      width: '250px',
      data: 'Shelve'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deleteShelve(bookCaseId, shelveId);
      }
    });
  }

  private deleteBook(bookCaseId: number, shelveId: number, bookId: number): void {
    this.bookService.delete(bookCaseId, shelveId, bookId)
      .subscribe(() => {
        this.snackBar.open('Book has been successfully deleted !', 'OK');
        this.listBooks(bookCaseId, shelveId);
      });
  }

  onDeleteBook(bookCaseId: number, shelveId: number, bookId: number): void {
    const dialogRef = this.deleteDialog.open(DeleteDialogComponent, {
      width: '250px',
      data: 'Book'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deleteBook(bookCaseId, shelveId, bookId);
      }
    });
  }

  openDialogEditBookCase(bookCase: Bookcase): void {
    const dialog = new MatDialogConfig();
    dialog.autoFocus = true;
    dialog.closeOnNavigation = true;
    dialog.data = {
      id: bookCase.id,
      name: bookCase.name
    };
    this.validationDialog.open(BookcaseDialogComponent, dialog)
      .afterClosed()
      .subscribe(() => this.listBookcases());
  }

  openDialogEditShelve(shelve: Shelve): void {
    const dialog = new MatDialogConfig();
    dialog.autoFocus = true;
    dialog.closeOnNavigation = true;
    dialog.data = {
      id: shelve.id,
      name: shelve.name,
      bookCase: {
        id: shelve.bookCase.id,
        name: shelve.bookCase.name
      }
    };
    this.validationDialog.open(ShelveDialogComponent, dialog)
      .afterClosed()
      .subscribe(() => this.listBookcases());
  }

  openDialogEditBook(book: Book): void {
    const dialog = new MatDialogConfig();
    dialog.autoFocus = true;
    dialog.closeOnNavigation = true;
    dialog.data = {
      id: book.id,
      title: book.title,
      author: book.author,
      description: book.description,
      shelve: {
        id: book.shelve.id,
        name: book.shelve.name,
        bookCase: {
          id: book.shelve.bookCase.id,
          name: book.shelve.bookCase.name
        }
      }

    };
    this.validationDialog.open(BookDialogComponent, dialog)
      .afterClosed()
      .subscribe(() => this.listBookcases());
  }
}
