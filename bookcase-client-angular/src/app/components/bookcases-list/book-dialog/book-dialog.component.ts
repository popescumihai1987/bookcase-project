import {Component, Inject, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {Bookcase} from "../../../common/bookcase";
import {BookcaseService} from "../../../services/bookcase.service";
import {Shelve} from "../../../common/shelve";
import {ShelveService} from "../../../services/shelve.service";
import {BookService} from "../../../services/book.service";
import {Book} from "../../../common/book";

@Component({
  selector: 'app-shelve-create',
  templateUrl: './book-dialog.component.html',
  styleUrls: ['./book-dialog.component.scss']
})
export class BookDialogComponent implements OnInit {
  bookForm: FormGroup;


  books: Book[];

  constructor(@Inject(MAT_DIALOG_DATA) public data: Book,
              private formBuilder: FormBuilder,
              private bookcaseService: BookcaseService,
              private shelveService: ShelveService,
              private bookService: BookService,
              private snackBar: MatSnackBar) {}

  ngOnInit(): void {
      this.initForm(this.data);
  }

  private initForm(book: Book): void {
    this.bookForm = this.formBuilder.group({
      bookTitle: [book.title, Validators.required],
      bookAuthor: [book.author, Validators.required],
      bookDesc: [book.description, Validators.required]
    });
  }

  get f(): { [key: string]: AbstractControl } {
    return this.bookForm.controls;
  }

  onSubmit(): void {
    const updateBook = new Book();
    updateBook.id = this.data.id;
    updateBook.title = this.bookForm.value.bookTitle;
    updateBook.author = this.bookForm.value.bookTitle;
    updateBook.description = this.bookForm.value.bookDesc;
    const shelve = new Shelve();
    shelve.id = this.data.shelve.id;
    shelve.name = this.data.shelve.name;
    const bookcase = new Bookcase();
    bookcase.id = this.data.shelve.bookCase.id;
    shelve.bookCase = bookcase;
    updateBook.shelve = shelve;
    this.bookService.update(this.data.shelve.bookCase.id, this.data.shelve.bookCase.id, updateBook)
      .subscribe(() => {
        this.snackBar.open('The book has been successfully updated !', 'OK');
      });
  }
}
