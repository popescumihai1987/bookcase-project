import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookcasesListComponent } from './bookcases-list.component';

describe('BookcasesListComponent', () => {
  let component: BookcasesListComponent;
  let fixture: ComponentFixture<BookcasesListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookcasesListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookcasesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
