import {Component, OnInit} from '@angular/core';
import {BookcaseService} from '../../services/bookcase.service';
import {Bookcase} from '../../common/bookcase';
import {Shelve} from '../../common/shelve';
import {ShelveService} from '../../services/shelve.service';
import {Book} from '../../common/book';
import {BookService} from '../../services/book.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {DeleteDialogComponent} from '../dialog/delete/delete-dialog.component';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {LooseBookService} from '../../services/loose-books.service';
import {LooseBookDialogComponent} from '../bookcases-list/loose-book-dialog/loose-book-dialog.component';

@Component({
  selector: 'app-loose-books-list',
  templateUrl: './loose-books-list.component.html',
  styleUrls: ['./loose-books-list.component.css']
})
export class LooseBooksListComponent implements OnInit {

  bookcases: Bookcase[];

  bookCaseName: string;

  shelves: Shelve[];

  shelveName: string;

  bookCaseId: number;

  books: Book[];

  bookTitle: string;

  constructor(private bookcaseService: BookcaseService,
              private shelveService: ShelveService,
              private looseBookService: LooseBookService,
              private bookService: BookService,
              private deleteDialog: MatDialog,
              private validationDialog: MatDialog,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.listBooks();
  }

  listBooks(): void {
    this.looseBookService.getAllLoose().subscribe(
      data => {
        this.books = data;
      }
    );
  }

  private deleteBookById(bookId: number): void {
    this.bookService.deleteBookById(bookId)
      .subscribe(() => {
        this.snackBar.open('Book has been successfully deleted !', 'OK');
        this.listBooks();
      });
  }

  onDeleteBook(bookId: number): void {
    const dialogRef = this.deleteDialog.open(DeleteDialogComponent, {
      width: '250px',
      data: 'Book'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deleteBookById(bookId);
      }
    });
  }

  openDialogEditBook(book: Book): void {
    const dialog = new MatDialogConfig();
    dialog.autoFocus = true;
    dialog.closeOnNavigation = true;
    dialog.data = {
      id: book.id,
      title: book.title,
      author: book.author,
      description: book.description
    };
    this.validationDialog.open(LooseBookDialogComponent, dialog)
      .afterClosed()
      .subscribe(() => this.listBooks());
  }

}
