import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LooseBooksListComponent } from './loose-books-list.component';

describe('LooseBooksListComponent', () => {
  let component: LooseBooksListComponent;
  let fixture: ComponentFixture<LooseBooksListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LooseBooksListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LooseBooksListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
