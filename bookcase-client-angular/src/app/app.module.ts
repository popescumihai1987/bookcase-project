import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BookcasesListComponent } from './components/bookcases-list/bookcases-list.component';
import { HttpClientModule } from '@angular/common/http';
import { BookcaseService } from './services/bookcase.service';
import { MatExpansionModule } from '@angular/material/expansion';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {DeleteDialogComponent} from './components/dialog/delete/delete-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {BookcaseDialogComponent} from './components/bookcases-list/bookcase-dialog/bookcase-dialog.component';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {ShelveDialogComponent} from './components/bookcases-list/shelve-dialog/shelve-dialog.component';
import {MatOptionModule} from '@angular/material/core';
import {BookDialogComponent} from './components/bookcases-list/book-dialog/book-dialog.component';
import {LooseBooksListComponent} from './components/loose-books-list/loose-books-list.component';
import {LooseBookDialogComponent} from './components/bookcases-list/loose-book-dialog/loose-book-dialog.component';
import {MatButtonModule} from "@angular/material/button";


@NgModule({
  declarations: [
    AppComponent,
    BookcasesListComponent,
    LooseBooksListComponent,
    DeleteDialogComponent,
    BookcaseDialogComponent,
    ShelveDialogComponent,
    BookDialogComponent,
    LooseBookDialogComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MatExpansionModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    MatSnackBarModule,
    MatDialogModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatButtonModule
  ],
  providers: [BookcaseService],
  bootstrap: [AppComponent]
})
export class AppModule { }
