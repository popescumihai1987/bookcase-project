import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Bookcase} from '../common/bookcase';

@Injectable({
  providedIn: 'root'
})
export class BookcaseService {

  private baseUrl = 'http://localhost:8888/';

  constructor(private httpClient: HttpClient) { }

  getAll(): Observable<Bookcase[]> {
    return this.httpClient.get<Bookcase[]>(this.baseUrl + 'bookcase');
  }

  getById(id: number): Observable<Bookcase> {
    return this.httpClient.get<Bookcase>(this.baseUrl + 'bookcase/' + id);
  }

  add(bookcase: Bookcase): Observable<Bookcase> {
    return this.httpClient.post<Bookcase>(this.baseUrl + 'bookcase', bookcase);
  }

  update(bookcase: Bookcase): Observable<Bookcase> {
    return this.httpClient.put<Bookcase>(this.baseUrl + 'bookcase/' + bookcase.id, bookcase);
  }

  delete(id: number): Observable<void> {
    return this.httpClient.delete<void>(this.baseUrl + 'bookcase/' + id);
  }

}
