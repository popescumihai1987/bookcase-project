import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Book} from '../common/book';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  private baseUrl = 'http://localhost:8888/';

  constructor(private httpClient: HttpClient) { }

  getAll(bookCaseId: number, shelveId: number): Observable<Book[]> {
    return this.httpClient.get<Book[]>(this.baseUrl + 'bookcase/' + bookCaseId + '/shelve/' + shelveId + '/books');
  }

  getById(bookCaseId: number, shelveId: number, id: number): Observable<Book> {
    return this.httpClient.get<Book>(this.baseUrl + 'bookcase/' + bookCaseId + '/shelve/' + shelveId + '/books/' + id);
  }

  add(bookCaseId: number, shelveId: number, book: Book): Observable<Book> {
    return this.httpClient.post<Book>(this.baseUrl + 'bookcase/' + bookCaseId + '/shelve/' + shelveId + '/books', book);
  }

  update(bookCaseId: number, shelveId: number, book: Book): Observable<Book> {
    return this.httpClient.put<Book>(this.baseUrl + 'bookcase/' + bookCaseId + '/shelve/' + shelveId + '/books/' + book.id, book);
  }

  delete(bookCaseId: number, shelveId: number, id: number): Observable<void> {
    return this.httpClient.delete<void>(this.baseUrl + 'bookcase/' + bookCaseId + '/shelve/' + shelveId + '/books/' + id);
  }

  deleteBookById(bookId: number): Observable<void> {
    return this.httpClient.delete<void>(this.baseUrl + 'bookcase/' + null + '/shelve/' + null + '/books/' + bookId);
  }
}
