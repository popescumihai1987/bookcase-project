import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Book} from '../common/book';

@Injectable({
  providedIn: 'root'
})
export class LooseBookService {

  private baseUrl = 'http://localhost:8888/';

  constructor(private httpClient: HttpClient) { }

  getAllLoose(): Observable<Book[]> {
    return this.httpClient.get<Book[]>(this.baseUrl + 'loosebooks/');
  }

  updateShelve(shelveId: number, book: Book): Observable<Book> {
    return this.httpClient.put<Book>(this.baseUrl + 'loosebooks/' + book.id, book);
  }

  update(book: Book): Observable<Book> {
    return this.httpClient.put<Book>(this.baseUrl + 'loosebooks/' + book.id, book);
  }

}
