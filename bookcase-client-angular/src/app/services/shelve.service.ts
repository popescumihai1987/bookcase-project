import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Shelve} from '../common/shelve';

@Injectable({
  providedIn: 'root'
})
export class ShelveService {

  private baseUrl = 'http://localhost:8888/';

  constructor(private httpClient: HttpClient) { }

  getAll(bookCaseId: number): Observable<Shelve[]> {
    return this.httpClient.get<Shelve[]>(this.baseUrl + 'bookcase/' + bookCaseId + '/shelve');
  }

  getById(bookCaseId: number, id: number): Observable<Shelve> {
    return this.httpClient.get<Shelve>(this.baseUrl + 'bookcase/' + bookCaseId + '/shelve/' + id);
  }

  add(bookCaseId: number, shelve: Shelve): Observable<Shelve> {
    return this.httpClient.post<Shelve>(this.baseUrl + 'bookcase/' + bookCaseId + '/shelve', shelve);
  }

  update(bookCaseId: number, shelve: Shelve): Observable<Shelve> {
    return this.httpClient.put<Shelve>(this.baseUrl + 'bookcase/' + bookCaseId + '/shelve/' + shelve.id, shelve);
  }

  delete(bookCaseId: number, id: number): Observable<void> {
    return this.httpClient.delete<void>(this.baseUrl + 'bookcase/' + bookCaseId + '/shelve/' + id);
  }
}
