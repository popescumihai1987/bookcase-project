import {Bookcase} from './bookcase';

export class Shelve {
  id: number;
  name: string;
  bookCase: Bookcase;
}
