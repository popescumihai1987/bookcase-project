import {Shelve} from './shelve';

export class Book {
  id: number;
  title: string;
  author: string;
  description: string;
  shelve: Shelve;
}
