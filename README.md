# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
  
    This project has been started as a Java 4(Spring) final project.
    The main idea of the app is organising and keeping track of your books on the shelves of your bookcases.
    The backend is Java, inspired from the course and from some junior tasks I have worked on at my job.
    The frontend is Angular as I needed to learn something new and it seemd like the best idea.
  
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
  
    bookcase -> run the sql database.sql with root on your mysql database
             -> clean install
             -> run com.bookcase.BookcaseApplication.java
  
    bookcase-client-angular -> install node
                            -> run 'ng serve --open' from bookcase-client-angular folder
  
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
